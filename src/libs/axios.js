import Vue from 'vue'

// axios
import axios from 'axios'

const axiosIns = axios.create({
  // You can add your headers here
  // ================================
  // baseURL: 'https://some-domain.com/api/',
  // timeout: 1000,
  // headers: {'X-Custom-Header': 'foobar'}
  baseURL: 'https://tiktok-downloader-download-tiktok-videos-without-watermark.p.rapidapi.com/',
  headers: {
    'X-RapidAPI-Host' : 'tiktok-downloader-download-tiktok-videos-without-watermark.p.rapidapi.com',
    'X-RapidAPI-Key' : 'e8e9b00871mshc41fa3a470377ebp1bc4a8jsndfcc2c817ac9',
  }

})

Vue.prototype.$http = axiosIns

export default axiosIns
